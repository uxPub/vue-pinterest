import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    photos: [],
  },
  mutations: {
    FETCH_PHOTOS(state, photos) {
      state.photos = photos
    }
  },
  actions: {
    fetchPhotos({ commit }, res){
      commit('FETCH_PHOTOS', res)
    }
  },
  modules: {
  }
})
