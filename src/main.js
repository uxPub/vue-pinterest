import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import vuetify from './plugins/vuetify'
import 'vue-stack-grid'
import 'vue-chartjs'
import TextHighlight from 'vue-text-highlight';

Vue.component('text-highlight', TextHighlight);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  axios,
  render: h => h(App)
}).$mount('#app')
