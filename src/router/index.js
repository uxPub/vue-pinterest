import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../pages/HomePage.vue')
  },
  {
    path: '/stat',
    name: 'StatPage',
    component: () => import('../pages/StatPage.vue')
  },
  {
    path: '/likes',
    name: 'LikesPage',
    component: () => import('../pages/LikesPage.vue')
  },
  {
    path: '/follwing',
    name: 'FollwingPage',
    component: () => import('../pages/FollwingPage.vue')
  },
  {
    path: '/ebook',
    name: 'EbookPage',
    component: () => import('../pages/EbookPage.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
