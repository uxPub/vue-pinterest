module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  configureWebpack: {        
    devServer: {
      headers: {
        'Access-Control-Allow-Origin': '*'        
      },
      proxy: {
        '^/oauth/': {
          target: 'https://unsplash.com',
          changeOrigin: true,
          secure:false,
          pathRewrite: {'^/oauth/': '/oauth/'},
          logLevel: 'debug'
        }
      }
    }
  },
}